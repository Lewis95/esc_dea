
library(nonparaeff)
cls_id=1:5
ext_name=".csv"
csv_path <- "./Population DEA Model/housing_removed/"
result_path <- "./ClusResults/"
cluster_size <- c(9,19,22,10,19)
prefix1="PopDEAClus"
prefix2="NH"
#prefix='LGPRFSharedDEACluster'

# Compute dea for mulitple clusters for a model 
compute_dea <- function(model_file_name,res_file_name,cls_size) 
{
  model_data <- read.csv(model_file_name)
  model_data_formatted <- data.frame(model_data[,3],model_data[,4],model_data[,5],model_data[,2],model_data[,1])
  
  model_crs <- dea(base = model_data_formatted, noutput = 3, orientation = 1, rts = 1)
  
  model_vrs <- dea(base = model_data_formatted, noutput = 3, orientation = 1, rts = 2 )
  
  # RTS
  rts <- rowSums(model_crs[,2:cls_size+1])
  rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))
  
  rts_final <- ifelse(model_crs[,1] >= 0.99, "Constant",rts_number)
  
  # Scale efficiency
  
  scale_efficiency <- model_crs[,1]/model_vrs[,1]
  
  # Output dataframe
  
  mode_output <- data.frame(model_crs[,1], model_vrs[,1], scale_efficiency, rts_final)
  
  colnames(mode_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")
  
  write.csv(mode_output, file = res_file_name, row.names = FALSE)
}


# compute cluster dea results for all the model 

for(i in cls_id)
{ 
  file_name=paste(prefix1,toString(i),prefix2,sep="")
  #file_name=paste(prefix,toString(i),sep="")
  input_file_path=paste(csv_path,file_name,ext_name,sep='')
  #print(input_file_path)
  #model_data <- read.csv(input_file_path)
  #cls_path_1 <- paste(csv_path,"Cluster",toString(i),sep = '')
  #model_file_name <- paste(cls_path_1,mid_name,model_name,sep = ' ')
  #model_file_name <- paste(paste(cls_path_1,mid_name,sep=" "),model_name,sep = '')
  res_file_name_path <- paste(result_path,file_name,"dea_resuls",ext_name,sep='')
  #print(res_file_name_path)
 
  compute_dea(input_file_path,res_file_name_path,cluster_size[i]) 
  
  
}



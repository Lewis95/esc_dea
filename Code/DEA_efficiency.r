library(nonparaeff)

# Model 1
# Build model1_data

model1_data <- read.csv('./Data/Clusters/Clusters Finalised/C2 Revised M4.csv')
 
model1_data_formatted <- data.frame(model1_data[,3],model1_data[,4],model1_data[,5],model1_data[,1],model1_data[,2])

model1_crs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 2 )
#cluster_size <- c(rep(c(9,19,22,10,19),7))

# RTS
cls_mem=20
rts <- rowSums(model1_crs[,2:cls_mem])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "./Results/C2_revised_M4_Dea_results.csv", row.names = FALSE)





# Model 7

# Build model1_data

model7_data <- read.csv("Model7.csv", header = TRUE)
 
#model7_data_formatted <- data.frame(model7_data[,3],model7_data[,4],model7_data[,5],model7_data[,1],model7_data[,2])

model7_crs <- dea(base = model7_data, noutput = 4, orientation = 1, rts = 1)

model7_vrs <- dea(base = model7_data, noutput = 4, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model7_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model7_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model7_crs[,1]/model7_vrs[,1]

# Output dataframe

model_output <- data.frame(model7_crs[,1], model7_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model7_dea_efficiency.csv", row.names = FALSE)

# Model 8

# Build model8_data

model1_data <- read.csv("Model8.csv", header = TRUE)
 
model1_data_formatted <- data.frame(model1_data[,3],model1_data[,4],model1_data[,5],model1_data[,1],model1_data[,2])

model1_crs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model1_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model8_dea_efficiency.csv", row.names = FALSE)

# Model 9

# Build model8_data

model1_data <- read.csv("Model9.csv", header = TRUE)
 
model1_data_formatted <- data.frame(model1_data[,3],model1_data[,4],model1_data[,5],model1_data[,6],model1_data[,1],model1_data[,2])

model1_crs <- dea(base = model1_data_formatted, noutput = 4, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 4, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model1_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model9_dea_efficiency.csv", row.names = FALSE)

# Model 9a

# Build model9a_data

model1_data <- read.csv("Model9a.csv", header = TRUE)
 
model1_data_formatted <- data.frame(model1_data[,3],model1_data[,4],model1_data[,5],model1_data[,1],model1_data[,2])

model1_crs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model1_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model9a_dea_efficiency.csv", row.names = FALSE)

# Model 10

# Build model10_data

model1_data <- read.csv("Model10.csv", header = TRUE)
 
model1_data_formatted <- data.frame(model1_data[,3],model1_data[,4],model1_data[,5],model1_data[,1],model1_data[,2])

model1_crs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 3, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model1_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model10_dea_efficiency.csv", row.names = FALSE)

# Model 6a

# Build model8_data

model1_data <- read.csv("Model6a.csv", header = TRUE)
 
model1_data_formatted <- data.frame(model1_data[,3],model1_data[,4],model1_data[,5],model1_data[,6],model1_data[,1],model1_data[,2])

model1_crs <- dea(base = model1_data_formatted, noutput = 4, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 4, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model1_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model6a_dea_efficiency.csv", row.names = FALSE)

# Model 7a

# Build model8_data

model1_data <- read.csv("Model7a.csv", header = TRUE)
 
model1_data_formatted <- data.frame(model1_data[,4],model1_data[,5],model1_data[,6],model1_data[,7],model1_data[,1],model1_data[,2],model1_data[,3])

model1_crs <- dea(base = model1_data_formatted, noutput = 4, orientation = 1, rts = 1)

model1_vrs <- dea(base = model1_data_formatted, noutput = 4, orientation = 1, rts = 2 )

# RTS

rts <- rowSums(model1_crs[,2:80])
rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

rts_final <- ifelse(model1_crs[,1] >= 0.99, "Constant",rts_number)

# Scale efficiency

scale_efficiency <- model1_crs[,1]/model1_vrs[,1]

# Output dataframe

model_output <- data.frame(model1_crs[,1], model1_vrs[,1], scale_efficiency, rts_final)

colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

write.csv(model_output, file = "model7a_dea_efficiency.csv", row.names = FALSE)

# Model all Cluster models

file_names <- c("Model1Cluster1.csv", "Model1Cluster2.csv", "Model1Cluster3.csv", "Model1Cluster4.csv", "Model1Cluster5.csv","Model2Cluster1.csv", "Model2Cluster2.csv", "Model2Cluster3.csv", "Model2Cluster4.csv", "Model2Cluster5.csv","Model3Cluster1.csv", "Model3Cluster2.csv", "Model3Cluster3.csv", "Model3Cluster4.csv", "Model3Cluster5.csv","Model4Cluster1.csv", "Model4Cluster2.csv", "Model4Cluster3.csv", "Model4Cluster4.csv", "Model4Cluster5.csv", "Model5Cluster1.csv", "Model5Cluster2.csv", "Model5Cluster3.csv", "Model5Cluster4.csv", "Model5Cluster5.csv", "Model6Cluster1.csv", "Model6Cluster2.csv", "Model6Cluster3.csv", "Model6Cluster4.csv", "Model6Cluster5.csv", "Model7Cluster1.csv", "Model7Cluster2.csv", "Model7Cluster3.csv", "Model7Cluster4.csv", "Model7Cluster5.csv")
n_outputs <- c(rep(3,5),rep(3,5),rep(3,5),rep(4,5),rep(1,5),rep(3,5),rep(4,5))
cluster_size <- c(rep(c(9,21,22,8,19),7))
model_count <- 1
cluster_count <- 0

for(i in 1:length(file_names))
{
	# Read in data
	model_data <- read.csv(file_names[i], header = TRUE)
	
	# Build models 
	model_crs <- dea(base = model_data, noutput = n_outputs[i], orientation = 1, rts = 1)

	model_vrs <- dea(base = model_data, noutput = n_outputs[i], orientation = 1, rts = 2 )
	
	# RTS

	rts <- rowSums(model_crs[,2:(cluster_size[i]+1)])
	rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

	rts_final <- ifelse(model_crs[,1] >= 0.99, "Constant",rts_number)

	# Scale efficiency

	scale_efficiency <- model_crs[,1]/model_vrs[,1]

	# Output dataframe

	if(cluster_count < 5)
	{
		cluster_count <- cluster_count+1
	} else {
		cluster_count <- 1
		model_count <- model_count+1
	}
	
	model_output <- data.frame(model_crs[,1], model_vrs[,1], scale_efficiency, rts_final)

	colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

	write.csv(model_output, file = paste("model", model_count, "_cluster", cluster_count, "_dea_efficiency.csv", sep = ""), row.names = FALSE)
}

# Cluster for models 8 and 9
file_names <- c("Model8Cluster1.csv", "Model8Cluster2.csv", "Model8Cluster3.csv", "Model8Cluster4.csv", "Model8Cluster5.csv","Model9Cluster1.csv", "Model9Cluster2.csv", "Model9Cluster3.csv", "Model9Cluster4.csv", "Model9Cluster5.csv")
n_outputs <- c(rep(3,5),rep(4,5))
cluster_size <- c(rep(c(9,21,22,8,19),7))
model_count <- 8
cluster_count <- 0

for(i in 1:length(file_names))
{
	# Read in data
	model_data <- read.csv(file_names[i], header = TRUE)
	
	# Build models 
	model_crs <- dea(base = model_data, noutput = n_outputs[i], orientation = 1, rts = 1)

	model_vrs <- dea(base = model_data, noutput = n_outputs[i], orientation = 1, rts = 2 )
	
	# RTS

	rts <- rowSums(model_crs[,2:(cluster_size[i]+1)])
	rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

	rts_final <- ifelse(model_crs[,1] >= 0.99, "Constant",rts_number)

	# Scale efficiency

	scale_efficiency <- model_crs[,1]/model_vrs[,1]

	# Output dataframe

	if(cluster_count < 5)
	{
		cluster_count <- cluster_count+1
	} else {
		cluster_count <- 1
		model_count <- model_count+1
	}
	
	model_output <- data.frame(model_crs[,1], model_vrs[,1], scale_efficiency, rts_final)

	colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

	write.csv(model_output, file = paste("model", model_count, "_cluster", cluster_count, "_dea_efficiency.csv", sep = ""), row.names = FALSE)
}

# Cluster for models 6a and 7a
file_names <- c("Model6aCluster1.csv", "Model6aCluster2.csv", "Model6aCluster3.csv", "Model6aCluster4.csv", "Model6aCluster5.csv","Model7aCluster1.csv", "Model7aCluster2.csv", "Model7aCluster3.csv", "Model7aCluster4.csv", "Model7aCluster5.csv")
n_outputs <- c(rep(4,5),rep(4,5))
cluster_size <- c(rep(c(9,21,22,8,19),7))
model_count <- 6
cluster_count <- 0

for(i in 1:length(file_names))
{
	# Read in data
	model_data <- read.csv(file_names[i], header = TRUE)
	
	# Build models 
	model_crs <- dea(base = model_data, noutput = n_outputs[i], orientation = 1, rts = 1)

	model_vrs <- dea(base = model_data, noutput = n_outputs[i], orientation = 1, rts = 2 )
	
	# RTS

	rts <- rowSums(model_crs[,2:(cluster_size[i]+1)])
	rts_number <- ifelse(rts > 1, "Decreasing", ifelse(rts < 1,"Increasing","Constant"))

	rts_final <- ifelse(model_crs[,1] >= 0.99, "Constant",rts_number)

	# Scale efficiency

	scale_efficiency <- model_crs[,1]/model_vrs[,1]

	# Output dataframe

	if(cluster_count < 5)
	{
		cluster_count <- cluster_count+1
	} else {
		cluster_count <- 1
		model_count <- model_count+1
	}
	
	model_output <- data.frame(model_crs[,1], model_vrs[,1], scale_efficiency, rts_final)

	colnames(model_output) <- c("TE (CRS)", "TE (VRS)", "Scale Efficiency Score", "RTS")

	write.csv(model_output, file = paste("model", model_count, "a_cluster", cluster_count, "_dea_efficiency.csv", sep = ""), row.names = FALSE)
}
